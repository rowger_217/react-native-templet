import React, { Component } from 'react';
import { View, Text,StyleSheet } from "react-native"

export default class MineIndex extends Component<{}> {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          MineIndex--我的
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#5080B4',
  },
  welcome: {
    fontSize: 30,
    textAlign: 'center',
    color: 'white',
  },
});
