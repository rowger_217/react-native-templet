// import  React , { Component } from 'react';
import { Dimensions, PixelRatio, Platform, Alert } from 'react-native';

import Images from '../assets/Images'


const { height, width } = Dimensions.get('window');
// 系统是iOS
global.iOS = (Platform.OS === 'ios');
// 系统是安卓
global.Android = (Platform.OS === 'android');
// 获取屏幕宽度
global.SCREEN_WIDTH = width;
// 获取屏幕高度
global.SCREEN_HEIGHT = height;
// 获取屏幕分辨率
global.PixelRatio = PixelRatio.get();
// 最小线宽
global.pixel = 1 / PixelRatio;

// 用户登录状态
global.TOKEN = false;


// 弹出框
global.Alert = Alert;

global.Images = Images;
//
global.NAV_BAR_HEIGHT_IOS = 44;
global.NAV_BAR_HEIGHT_ANDROID = 50;
global.STATUS_BAR_HEIGHT = 20;
global.NAV_BAR_HEIGHT = global.iOS ? global.NAV_BAR_HEIGHT_IOS : global.NAV_BAR_HEIGHT_ANDROID
