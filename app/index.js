import React, { Component } from 'react';
import { View, Text,StyleSheet,Platform } from "react-native"
import {StackNavigator,TabNavigator,TabBarBottom} from 'react-navigation';
import CardStackStyleInterpolator from 'react-navigation/src/views/CardStack/CardStackStyleInterpolator';
import {HomeIndex,OrderList,MessageIndex} from './Routers/index'
import TabBarItem from './Commons/tabBarItem'
import theme from './Commons/color'

import ShopIndex from './ShopIndex'
import MineIndex from './MineIndex';
import MaizeScreen from './MaizeScreen';
import GoldScreen from './GoldScreen';
import BlackScreen from './BlackScreen';
import TabIcon from './Component/TabIcon';

// 全局注册并注入mobx，首页新品，分类页，商品详情页，购物车页面都要用到store
import {Provider} from 'mobx-react'
// 获取store实例
import store from './mobx/store'



export default class App extends Component<{}> {
  render() {
    return (
      <Provider rootStore={store}>
        <Navigator/>
      </Provider>
    );
  }
}
const  Navigation = () => {
  return (
    <Provider rootStore={store} >
      <Navigator/>
    </Provider>
  );
}
// TabNavigator
const Tab = TabNavigator(
  {
    Home:{
      screen:HomeIndex,
      navigationOptions:({navigation})=>(
        {
          tabBarLabel:'主页',
          tabBarIcon: ({focused, tintColor }) =>
            (
              <TabBarItem
                tintColor={tintColor}
                focused={focused}
                selectedImage = {require('./img/homeSelect.png')}
                normalImage = {require('./img/home.png')}
              />
            )
        }
      )
    }
  },

  // tabScreen配置
  {
    tabBarComponent:TabBarBottom, // 自定义
    tabBarPosition:'bottom',
    swipeEnabled:false,
    animationEnabled:true,
    lazy:true,
    tabBarOptions:{
      activeTintColor: theme.color,
      inactiveTintColor:'#979797',
      labelStyle: {
        fontSize: 12, // 文字大小
      },
    }

  }


)

const Navigator = StackNavigator(

  {
    HomeIndex:{screen: HomeIndex},
    OrderList:{screen: OrderList},
    MessageIndex:{screen: MessageIndex}
  },

  {
    navigationOptions:{
      // 开启动画
      animationEnabled:true,
      // 开启边缘触摸返回
      gesturesEnabled:true
    },
    mode:'card',
    transitionConfig:()=>({
      // 统一安卓和苹果页面跳转的动画
      screenInterpolator: CardStackStyleInterpolator.forHorizontal,
    })
  });

const styles = StyleSheet.create({
    tabBarStyle: {
        backgroundColor: '#eee',
        height:49,
    },
});
