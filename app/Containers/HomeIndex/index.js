import React, { Component } from 'react';
import { View, Text,StyleSheet } from "react-native"
import {OrderList} from "../../Routers";



export default class HomeIndex extends Component<{}> {
 handlr=()=>{
   console.log(this.props.navigation.navigate)
   this.props.navigation.navigate('OrderList')
 }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}
          onPress={()=>this.handlr()}
        >
          HomeIndex--首页22
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#68BBB0',
  },
  welcome: {
    fontSize: 30,
    textAlign: 'center',
    color: 'white'
  },
});
