import React, { Component } from 'react';
import { View, Text,StyleSheet } from "react-native"



export default class MessageIndex extends Component<{}> {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          MessageIndex
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#68BBB0',
  },
  welcome: {
    fontSize: 30,
    textAlign: 'center',
    color: 'white'
  },
});
